use clap::Parser;
use slot_checkout_stage::SlotId;

#[derive(Parser, Clone, Debug)]
struct Args {
    #[clap(value_parser = parse_slot_id)]
    slot: SlotId,
    project: String,
}

fn main() {
    let args = Args::parse();
    println!("would check out {} for {}", args.project, args.slot);
}

fn parse_slot_id(value: &str) -> Result<SlotId, String> {
    SlotId::try_from(value)
}
