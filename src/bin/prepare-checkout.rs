use clap::Parser;
use slot_checkout_stage::SlotId;

#[derive(Parser, Clone, Debug)]
struct Args {
    #[clap(value_parser = parse_slot_id)]
    slot: SlotId,
}

fn main() {
    let args = Args::parse();
    println!("setting up checkout for {}", args.slot);
    println!("pretend projects are: Gaudi Detector LHCb");
}

fn parse_slot_id(value: &str) -> Result<SlotId, String> {
    SlotId::try_from(value)
}
