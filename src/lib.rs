use std::fmt::Display;

#[derive(Debug, Clone)]
pub struct SlotId {
    pub flavour: String,
    pub name: String,
    pub build_id: u32,
}

impl SlotId {
    pub fn new(flavour: String, name: String, build_id: u32) -> Self {
        Self {
            flavour,
            name,
            build_id,
        }
    }
}

impl TryFrom<&str> for SlotId {
    type Error = String;
    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let chunks: Vec<&str> = value.split('/').collect();
        if chunks.len() != 3 {
            Err("it should be '<flavour>/<name>/<build_id>'".to_string())
        } else {
            Ok(Self::new(
                chunks[0].to_string(),
                chunks[1].to_string(),
                chunks[2]
                    .parse()
                    .map_err(|_err| format!("invalid number: '{}'", chunks[2]))?,
            ))
        }
    }
}

impl Display for SlotId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}/{}/{}", self.flavour, self.name, self.build_id)
    }
}
